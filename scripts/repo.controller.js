/**
 * Created by fathin on 7/18/2017.
 */
(function () {
    'use strict';
    angular.module('angularhub')
        .controller('RepoCtrl',
            [
                '$scope',
                '$rootScope',
                '$state',
                '$document',
                '$timeout',
                '$log',
                'AngularHubFactories',
                function($scope, $rootScope, $state, $document, $timeout, $log, AngularHubFactories){
                    $log.info("total repositori = " + $scope.gitHubData.public_repos);
                    $scope.storedRepos = [];
                    $scope.perPage = 25;
                    $scope.currentTotalRepos = 0;
                    $scope.totalRepos = $scope.gitHubData.public_repos;
                    $scope.pagination = {
                        current: 1
                    };
                    $scope.pageChanged = function(newPage) {
                        getResultsPage(newPage);
                    };
                    AngularHubFactories.doGet({url:'/users/andhikamaheva/repos?page=1&per_page='+$scope.perPage})
                        .then(
                            function(ok){
                                var totalRepos = $scope.currentTotalRepos + ok.data.length;
                                $scope.storedRepos = ok.data;
                                $scope.currentTotalRepos = totalRepos;
                            },
                            function(fail){
                                $log.error("loading repositories fail");
                            }
                        );
                    function getResultsPage(pageNumber) {
                        // this is just an example, in reality this stuff should be in a service
                        AngularHubFactories.doGet({url:'/users/andhikamaheva/repos?page='+pageNumber+'&per_page='+$scope.perPage})
                            .then(
                                function(ok){
                                    var totalRepos = $scope.currentTotalRepos + ok.data.length;
                                    $scope.storedRepos = ok.data;
                                    $scope.currentTotalRepos = totalRepos;
                                },
                                function(fail){
                                    $log.error("loading repositories fail");
                                }
                            );
                    }
                }
            ]
        );
})();