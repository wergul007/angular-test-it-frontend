/**
 * Created by fathin on 7/18/2017.
 */
(function () {
    'use strict';

    angular.module('angularhub')
        .run(
            [
                '$rootScope', '$state', '$stateParams', '$window', '$timeout', '$log',
                function($rootScope, $state, $stateParams, $window, $timeout, $log, OAuth) {
                    $rootScope.$state = $state;
                    $rootScope.$stateParams = $stateParams;
                    $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {

                    });
                    $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {

                    });
                }
            ]
        );

})();