/**
 * Created by fathin on 7/18/2017.
 */
(function () {
    'use strict';

    angular.module('angularhub')
        .config(['cfpLoadingBarProvider', function(cfpLoadingBarProvider){
            cfpLoadingBarProvider.includeSpinner = false;
        }])
        .config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
                var tplDir, tplExt, states, modDir;
                tplDir = 'views';
                tplExt = '.html';
                modDir = '/modules';

                $urlRouterProvider
                    .when('/', '/home')
                    .otherwise('/home');

                $stateProvider
                    .state("home", {
                        url: '/home',
                        name: 'home',
                        controller: 'HomeCtrl',
                        templateUrl: tplDir + '/home/home' + tplExt,
                        data:{
                            pageTitle: 'Home'
                        }
                    })
                    .state("repos", {
                        url:'/repos',
                        name:'repos',
                        controller:'RepoCtrl',
                        templateUrl: tplDir + '/home/repo' + tplExt,
                        data:{
                            pageTitle:'Repositories'
                        }
                    })
                    .state("stars",{
                        url:'/stars',
                        name:'stars',
                        controller:'StarCtrl',
                        templateUrl: tplDir + '/home/star' + tplExt,
                        data:{
                            pageTitle:'Stars'
                        }
                    })
                    .state("followers", {
                        url:'/followers',
                        name:'followers',
                        controller:'FollowerCtrl',
                        templateUrl: tplDir + '/home/follower' + tplExt,
                        data:{
                            pageTitle:'Followers'
                        }
                    })
                    .state("following", {
                        url:'/following',
                        name:'following',
                        controller:'FollowingCtrl',
                        templateUrl: tplDir + '/home/following' + tplExt,
                        data:{
                            pageTitle:'Following'
                        }
                    });
            }]
        );

})();