/**
 * Created by fathin on 7/18/2017.
 */
(function () {
    'use strict';
    angular.module(
        'angularhub.core',
        [
            'ui.router',
            'angularUtils.directives.dirPagination',
            'angular-loading-bar',
        ]
    );
})();