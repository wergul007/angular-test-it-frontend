/**
 * Created by fathin on 7/18/2017.
 */
(function () {
    'use strict';

    angular.module('angularhub')
        .factory('AngularHubFactories', ['$http', '$log', function($http, $log){
            var serviceProviderUrl = 'https://api.github.com';
            return {
                doGet: function(param){
                    return $http({
                        url: serviceProviderUrl + param.url,
                        method: 'GET',
                        headers: {
                            'Accept':'application/json',
                            'Content-Type':'application/json'
                        },
                        data: param.data
                    });
                }
            };
        }]);
})();