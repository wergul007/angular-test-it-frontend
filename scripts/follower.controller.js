/**
 * Created by fathin on 7/18/2017.
 */
(function () {
    'use strict';
    angular.module('angularhub')
        .controller('FollowerCtrl',
            [
                '$scope',
                '$rootScope',
                '$state',
                '$document',
                '$timeout',
                '$log',
                'AngularHubFactories',
                function($scope, $rootScope, $state, $document, $timeout, $log, AngularHubFactories){
                    $log.info("total followers = " + $scope.gitHubData.followers);
                    $scope.storedFollowers = [];
                    $scope.perPage = 25;
                    $scope.currentTotalFollowers = 0;
                    $scope.totalFollowers = $scope.gitHubData.followers;
                    $scope.pagination = {
                        current: 1
                    };
                    $scope.pageChanged = function(newPage) {
                        getResultsPage(newPage);
                    };
                    AngularHubFactories.doGet({url:'/users/andhikamaheva/followers?page=1&per_page='+$scope.perPage})
                        .then(
                            function(ok){
                                var totalData = $scope.currentTotalFollowers + ok.data.length;
                                $scope.storedFollowers = ok.data;
                                $scope.currentTotalFollowers = totalData;
                            },
                            function(fail){
                                $log.error("loading repositories fail");
                            }
                        );
                    function getResultsPage(pageNumber) {
                        // this is just an example, in reality this stuff should be in a service
                        AngularHubFactories.doGet({url:'/users/andhikamaheva/followers?page='+pageNumber+'&per_page='+$scope.perPage})
                            .then(
                                function(ok){
                                    var totalData= $scope.currentTotalFollowers + ok.data.length;
                                    $scope.storedFollowers = ok.data;
                                    $scope.currentTotalFollowers = totalData;
                                },
                                function(fail){
                                    $log.error("loading repositories fail");
                                }
                            );
                    }
                }
            ]
        );
})();