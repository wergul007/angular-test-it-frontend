/**
 * Created by fathin on 7/18/2017.
 */
(function () {
    'use strict';
    angular.module('angularhub')
        .controller('StarCtrl',
            [
                '$scope',
                '$rootScope',
                '$state',
                '$document',
                '$timeout',
                '$log',
                'AngularHubFactories',
                function($scope, $rootScope, $state, $document, $timeout, $log, AngularHubFactories){
                    $log.info("total repositori = " + 150);
                    $scope.storedRepos = [];
                    $scope.perPage = 25;
                    $scope.currentTotalRepos = 0;
                    $scope.totalRepos = 150;
                    $scope.pagination = {
                        current: 1
                    };
                    $scope.pageChanged = function(newPage) {
                        getResultsPage(newPage);
                    };
                    AngularHubFactories.doGet({url:'/users/andhikamaheva/starred?page=1&per_page='+$scope.perPage})
                        .then(
                            function(ok){
                                var totalRepos = $scope.currentTotalRepos + ok.data.length;
                                $scope.storedRepos = ok.data;
                                $scope.currentTotalRepos = totalRepos;
                            },
                            function(fail){
                                $log.error("loading repositories fail");
                            }
                        );
                    function getResultsPage(pageNumber) {
                        // this is just an example, in reality this stuff should be in a service
                        AngularHubFactories.doGet({url:'/users/andhikamaheva/starred?page='+pageNumber+'&per_page='+$scope.perPage})
                            .then(
                                function(ok){
                                    var totalRepos = $scope.currentTotalRepos + ok.data.length;
                                    $scope.storedRepos = ok.data;
                                    $scope.currentTotalRepos = totalRepos;
                                },
                                function(fail){
                                    $log.error("loading repositories fail");
                                }
                            );
                    }
                }
            ]
        );
})();